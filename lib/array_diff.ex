defmodule ArrayDiff do
  def array_diff(a, b) do
    res = a -- b
    l = b |> Enum.map(fn e -> Enum.member?(res, e) end) |> Enum.filter(fn e -> e end)
    cond do
      Enum.count(l) > 0 -> array_diff(res, b)
      true -> res
    end
  end
end